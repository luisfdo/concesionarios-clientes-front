import React, { useEffect, useState } from 'react';
import Grid from '@mui/material/Grid';
import { DataGrid } from '@mui/x-data-grid';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Form from '../Form/Form';
import axios from 'axios';



const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};


function Table() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [data, setData] = useState([]);


  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    try {
      const response = await axios.get('http://localhost:8090/concesionarios-clientes/api/customer/getAll');
      const responseData = response.data;
      setData(responseData);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  }

  async function fetchDelete(id) {
    try {
      await axios.delete('http://localhost:8090/concesionarios-clientes/api/customer/delete/'+id);
      fetchData();
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  }


  const handleAddClient = () => {
    fetchData();
    handleClose();
  };

  const columns = [
    { field: 'customerId', headerName: 'Id', width: 70 },
    { field: 'firstName', headerName: 'Primer Nombre', width: 130 },
    { field: 'secondName', headerName: 'Segundo Nombre', width: 130 },
    { field: 'surname', headerName: 'Primer Apellido', width: 130 },
    { field: 'secondSurname', headerName: 'Segundo Apellido', width: 130 },
    { field: 'documentNumber', headerName: 'Numero Documento', width: 130 },
    { field: 'cellPhoneNumber', headerName: 'Numero Celular', width: 130 },
    { field: 'address', headerName: 'Dirección', width: 180 },
    {
      field: "Inactivar",
      renderCell: (cellValues) => {
          return(
            <IconButton 
              aria-label="delete"
              onClick={(event) =>{
                handleDelete(event, cellValues);
              }}
            >
              <DeleteIcon />
            </IconButton>
          );
      }
    }
  ];

  const handleDelete = (event, cellValues) => {
    console.log(cellValues.row.customerId);
    fetchDelete(cellValues.row.customerId)
  }

  const getRowId = (row) => row.customerId; 

  return (
    <div style={{
      textAlign: "center",
      maxWidth: "fit-content",
      margin: "0 auto",
      padding: "40px 25px",
      marginTop: "50px"
    }}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <div style={{
            float: "right"
          }}>
            <Button onClick={handleOpen} variant="contained" endIcon={<AddIcon  />}>
              Crear
            </Button>
          </div>
        </Grid>
        
        <Grid item xs={12}>
          <DataGrid
            columns={columns}
            rows={data}
            getRowId={getRowId} // Especifica la función getRowId
            initialState={{
              pagination: {
                paginationModel: { page: 0, pageSize: 5 },
              },
            }}
            pageSizeOptions={[5, 10]}
          />
        </Grid>

        
      </Grid>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Creación clientes
          </Typography>
          <Form onAddClient={handleAddClient} />
        </Box>
      </Modal>
    </div>

    
  )
}


export default Table;