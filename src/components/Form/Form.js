import React, { Fragment } from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

import {
  Paper,
  Box,
  Grid,
  TextField,
  Typography,
  Button
} from '@material-ui/core';

function Form({ open, onClose, onAddClient }) {

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('Primer nombre es requerido'),
    secondName: Yup.string().required('Segundo nombre es requerido'),
    surname: Yup.string().required('Primer apellido es requerido'),
    secondSurname: Yup.string().required('Segundo apellido es requerido'),
    documentNumber: Yup.string().required('El numero del documento es requerido'),
    cellPhoneNumber: Yup.string().required('El numero del celular es requerido'),
    address: Yup.string().required('La dirección es requerida'),
  });

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: yupResolver(validationSchema)
  });

  const onSubmit = async data => {
    try {
      await axios.post('http://localhost:8090/concesionarios-clientes/api/customer/save', data);
      onAddClient();
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  

  return (
    <div style={{
      textAlign: "center",
      maxWidth: "950px",
      margin: "0 auto",
      padding: "40px 25px",
      marginTop: "50px",
    }}>
      <Fragment>
        <Paper>
          <Box px={3} py={2}>
            <Grid container spacing={1}>
              
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="firstName"
                  name="firstName"
                  label="Primer nombre"
                  margin="dense"
                  {...register('firstName')}
                  error={errors.username ? true : false}
                />
                <Typography variant="inherit" color="textSecondary">
                  {errors.firstName?.message}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="secondName"
                  name="secondName"
                  label="Segundo nombre"
                  margin="dense"
                  {...register('secondName')}
                  error={errors.username ? true : false}
                />
                <Typography variant="inherit" color="textSecondary">
                  {errors.secondName?.message}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="surname"
                  name="surname"
                  label="Primer apellido"
                  margin="dense"
                  {...register('surname')}
                  error={errors.username ? true : false}
                />
                <Typography variant="inherit" color="textSecondary">
                  {errors.surname?.message}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="secondSurname"
                  name="secondSurname"
                  label="Segundo apellido"
                  margin="dense"
                  {...register('secondSurname')}
                  error={errors.username ? true : false}
                />
                <Typography variant="inherit" color="textSecondary">
                  {errors.secondSurname?.message}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="cellPhoneNumber"
                  name="cellPhoneNumber"
                  label="Numero celular"
                  margin="dense"
                  {...register('cellPhoneNumber')}
                  error={errors.username ? true : false}
                />
                <Typography variant="inherit" color="textSecondary">
                  {errors.cellPhoneNumber?.message}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="address"
                  name="address"
                  label="Dirección"
                  margin="dense"
                  {...register('address')}
                  error={errors.username ? true : false}
                />
                <Typography variant="inherit" color="textSecondary">
                  {errors.address?.message}
                </Typography>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="documentNumber"
                  name="documentNumber"
                  label="Numero documento"
                  margin="dense"
                  {...register('documentNumber')}
                  error={errors.username ? true : false}
                />
                <Typography variant="inherit" color="textSecondary">
                  {errors.documentNumber?.message}
                </Typography>
              </Grid>
            </Grid>

            <Box mt={3}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleSubmit(onSubmit)}
              >
                Register
              </Button>
            </Box>
          </Box>
        </Paper>
      </Fragment>
    </div>
  );
}


export default Form;